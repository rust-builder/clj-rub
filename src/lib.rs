//! Rust Builder for [Clojure](https://github.com/clojure/clojure).
//!
//! This crate is intended for use with
//! [rub](https://github.com/rust-builder/rub).
//!
//! If you don't have `rub` installed, visit https://github.com/rust-builder/rub
//! for installation instructions.
//!
//! # Rub Options
//! <pre>
//! $ rub clojure --help
//! clojure - Rust Builder
//!
//! Usage:
//!     rub clj [options] [&lt;lifecycle&gt;...]
//!     rub clj (-h | --help)
//!     rub clj --version
//!
//! Options:
//!     -d --dir &lt;dir&gt;        Set the projects directory.
//!     -b --branch &lt;branch&gt;  Set the build branch. [default: master]
//!     -t --enable-test      Enable tests.
//!     -p --prefix &lt;prefix&gt;  Set the installation prefix.
//!     -u --url &lt;url&gt;        Set the SCM URL.
//!     -h --help             Show this usage.
//!     --dist                Create a distribution zip.
//!     --version             Show rust-rub version.
//! </pre>
//!
//! # Examples
//! ```rust
//! # extern crate buildable; extern crate clj_rub; fn main() {
//! use buildable::Buildable;
//! use clj_rub::CljRub;
//!
//! // To run lifecycle methods outside of rub...
//! let mut cr = CljRub::new();
//! let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
//!                                       "clj".to_string(),
//!                                       "--version".to_string()]);
//! assert_eq!(Ok(0), b.version());
//! # }
//! ```
#![experimental]
#![allow(unstable)]
extern crate buildable;
extern crate commandext;
extern crate docopt;
extern crate "rustc-serialize" as rustc_serialize;
extern crate scm;
extern crate utils;

use buildable::{Buildable,BuildConfig,LifeCycle};
use commandext::{CommandExt,to_res};
use scm::git::GitCommand;
use utils::empty::to_opt;
use docopt::Docopt;
use std::default::Default;
use std::io::fs;
use std::io::fs::PathExtensions;

static USAGE: &'static str = "clojure - Rust Builder

Usage:
    rub clj [options] [<lifecycle>...]
    rub clj (-h | --help)
    rub clj --version

Options:
    -d --dir <dir>        Set the projects directory.
    -b --branch <branch>  Set the build branch. [default: master]
    -t --enable-test      Enable tests.
    -p --prefix <prefix>  Set the installation prefix.
    -u --url <url>        Set the SCM URL.
    -h --help             Show this usage.
    --dist                Create a distribution zip.
    --version             Show rust-rub version.";

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[derive(RustcDecodable)]
struct Args {
    flag_dir: String,
    flag_branch: String,
    flag_enable_test: bool,
    flag_prefix: String,
    flag_url: String,
    flag_version: bool,
    flag_help: bool,
    flag_dist: bool,
    arg_lifecycle: Vec<String>,
}

#[cfg(target_os = "linux")]
fn os_clean(wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("mvn");
    cmd.wd(wd);
    cmd.header(true);
    cmd.args(&["clean"]);
    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_clean(wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("mvn.bat");
    cmd.wd(wd);
    cmd.header(true);
    cmd.args(&["clean"]);
    cmd.exec(to_res())
}

#[cfg(target_os = "linux")]
fn os_make(test: bool, wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("mvn");
    cmd.wd(wd);
    cmd.header(true);

    if test {
        cmd.args(&["package"]);
    } else {
        cmd.args(&["package", "-Dmaven.test.skip=true"]);
    }
    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_make(test: bool, wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("mvn.bat");
    cmd.wd(wd);
    cmd.header(true);

    if test {
        cmd.args(&["package"]);
    } else {
        cmd.args(&["package", "-Dmaven.test.skip=true"]);
    }
    cmd.exec(to_res())
}

#[cfg(target_os = "linux")]
fn os_install(dist: bool, test: bool, wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("mvn");
    cmd.wd(wd);
    cmd.header(true);

    if dist && test {
        cmd.args(&["package", "-Pdistribution"]);
    } else if dist && !test {
        cmd.args(&["package", "-Pdistribution", "-Dmaven.test.skip=true"]);
    } else if test {
        cmd.args(&["install"]);
    } else {
        cmd.args(&["install", "-Dmaven.test.skip=true"]);
    }

    cmd.exec(to_res())
}

#[cfg(not(target_os = "linux"))]
fn os_install(dist: bool, test: bool, wd: &Path) -> Result<u8,u8> {
    let mut cmd = CommandExt::new("mvn.bat");
    cmd.wd(wd);
    cmd.header(true);

    if dist && test {
        cmd.args(&["package", "-Pdistribution"]);
    } else if dist && !test {
        cmd.args(&["package", "-Pdistribution", "-Dmaven.test.skip=true"]);
    } else if test {
        cmd.args(&["install"]);
    } else {
        cmd.args(&["install", "-Dmaven.test.skip=true"]);
    }

    cmd.exec(to_res())
}

/// Clojure specific configuration for the `Buildable` lifecycle methods.
#[experimental]
#[derive(Clone,Default)]
pub struct CljRub {
    config: BuildConfig,
    prefix: String,
    url: String,
    dist: bool,
}

impl CljRub {
    /// Create a new default CljRub.
    pub fn new() -> CljRub {
        Default::default()
    }
}

impl Buildable for CljRub {
    /// Update the CljRub struct after parsing the given args vector.
    ///
    /// Normally, the args vector would be supplied from the command line, but
    /// they can be supplied as in the example below as well.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate clj_rub; fn main() {
    /// use buildable::Buildable;
    /// use clj_rub::CljRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = CljRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "clj".to_string()]);
    /// assert_eq!(Ok(0), b.version());
    /// # }
    fn new(&mut self, args: &Vec<String>) -> &mut CljRub {
        let dargs: Args = Docopt::new(USAGE)
            .and_then(|d| Ok(d.help(false)))
            .and_then(|d| d.argv(args.clone().into_iter()).decode())
            .unwrap_or_else( |e| e.exit());

        self.prefix = dargs.flag_prefix;
        self.url = dargs.flag_url;
        self.dist = dargs.flag_dist;

        if dargs.flag_version {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["version"]);
            self.config = cfg;
        } else if dargs.flag_help {
            let mut cfg = BuildConfig::new();
            cfg.lifecycle(vec!["help"]);
            self.config = cfg;
        } else {
            let mut cfg = BuildConfig::new();

            if to_opt(dargs.flag_dir.as_slice()).is_some() {
                cfg.dir(Path::new(dargs.flag_dir.as_slice()));
            }
            cfg.project("clojure");
            if to_opt(dargs.flag_branch.as_slice()).is_some() {
                cfg.branch(dargs.flag_branch.as_slice());
            }
            cfg.test(dargs.flag_enable_test);

            let lc = dargs.arg_lifecycle;
            if to_opt(lc.clone()).is_some() {
                let mut mylc = Vec::new();
                for lc in lc.iter() {
                    mylc.push(lc.as_slice());
                }
                cfg.lifecycle(mylc);
            }
            self.config = cfg;
        }
        self
    }

    /// Get the `BuildConfig` associated with the `CljRub`.
    ///
    /// # Example
    /// ```rust
    /// # extern crate buildable; extern crate clj_rub; fn main() {
    /// use buildable::Buildable;
    /// use clj_rub::CljRub;
    ///
    /// // To run lifecycle methods outside of rub...
    /// let mut cr = CljRub::new();
    /// let b = Buildable::new(&mut cr, &vec!["rub".to_string(),
    ///                                       "clj".to_string()]);
    /// let bc = b.get_bc();
    /// assert_eq!("clojure", bc.get_project());
    /// # }
    fn get_bc(&self) -> &BuildConfig {
        &self.config
    }

    /// No lifecycle reorder is necessary for Clojure.
    fn reorder<'a>(&self, lc: &'a mut Vec<LifeCycle>) -> &'a mut Vec<LifeCycle> {
        lc
    }

    /// Check for clojure dependencies.
    ///
    /// TODO: Implement
    fn chkdeps(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Peform the git operations necessary to get the project directory ready
    /// for the rest of the build lifecycle operations.
    ///
    /// # Notes
    /// * If the project directory doesn't exist, `clojure` will be cloned from
    /// github automatically.  You can adjust where is is cloned from by using
    /// the `--url` flag at the command line.
    /// * If the project does exist, the requested branch is updated via
    /// `update_branch` to prepare for the rest of the build cycle.
    fn scm(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let base = Path::new(cfg.get_dir());
        let u = if self.url.is_empty() {
            "git@github.com:clojure/clojure.git"
        } else {
            self.url.as_slice()
        };

        let mut cmd = GitCommand::new();
        cmd.wd(base.clone());
        cmd.verbose(true);

        if !base.join(cfg.get_project()).exists() {
            cmd.clone(Some(vec!["--recursive", u]), to_res())
        } else {
            Ok(0)
        }.and({
            cmd.wd(base.join(cfg.get_project()));
            cmd.update_branch(self.config.get_branch())
        })
    }

    /// Run `mvn clean` in the project directory.
    fn clean(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        os_clean(&wd)
    }

    /// Remove the dirty marker.
    fn configure(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        fs::unlink(&Path::new(wd.join("dirty"))).unwrap_or_else(|why| {
            println!("{}", why);
        });
        Ok(0)
    }

    /// Run `mvn package` in the project directory.
    fn make(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        os_make(cfg.get_test(), &wd)
    }

    /// Not implemented in the crate.
    ///
    /// # Notes
    /// * Testing is done during the `make` or `install` lifecycle steps
    /// through `maven`.  You can still control this with the `-t` flag.
    fn test(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Run `mvn install` or `mvn package -Pdistribution` based on the
    /// `--dist` flag.
    fn install(&self) -> Result<u8,u8> {
        let cfg = &self.config;
        let wd = Path::new(cfg.get_dir()).join(cfg.get_project());
        os_install(self.dist, cfg.get_test(), &wd)
    }

    /// Not implemented in this crate.
    fn cleanup(&self) -> Result<u8,u8> {
        Ok(0)
    }

    /// Show the docopt USAGE string on stdout.
    fn help(&self) -> Result<u8,u8> {
        println!("{}", USAGE);
        Ok(0)
    }

    /// Show the crate version on stdout.
    fn version(&self) -> Result<u8,u8> {
        println!("{} {} clj-rub {}", now(), sha(), branch());
        Ok(0)
    }
}

#[cfg(test)]
mod test {
    use buildable::{Buildable,BuildConfig};
    use super::CljRub;

    fn check_cr(cr: &CljRub) {
        assert_eq!(cr.prefix, "");
        assert_eq!(cr.url, "");
        assert!(!cr.dist);
    }

    fn check_bc(bc: &BuildConfig, lc: &Vec<&str>) {
        let mut tdir = env!("HOME").to_string();
        tdir.push_str("/projects");
        assert_eq!(bc.get_lifecycle(), lc);
        assert_eq!(bc.get_dir().as_str().unwrap(), tdir.as_slice());
        assert_eq!(bc.get_project(), "clojure");
        assert_eq!(bc.get_branch(), "master");
        assert!(!bc.get_test());
    }

    #[test]
    fn test_new() {
        let cr = CljRub::new();
        check_cr(&cr);
    }

    #[test]
    fn test_version() {
        let args = vec!["rub".to_string(),
                        "clj".to_string(),
                        "--version".to_string()];
        let mut cr = CljRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["version"]);
        assert_eq!(b.version(), Ok(0))
    }

    #[test]
    fn test_help() {
        let args = vec!["rub".to_string(),
                        "clj".to_string(),
                        "-h".to_string()];
        let mut cr = CljRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        assert_eq!(bc.get_lifecycle(), &vec!["help"]);
        assert_eq!(b.help(), Ok(0))
    }

    #[test]
    fn test_base() {
        let args = vec!["rub".to_string(),
                        "clj".to_string()];
        let mut cr = CljRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["most"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_scm() {
        let args = vec!["rub".to_string(),
                        "clj".to_string(),
                        "scm".to_string()];
        let mut cr = CljRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["scm"]);
        assert_eq!(b.version(), Ok(0));
    }

    #[test]
    fn test_all() {
        let args = vec!["rub".to_string(),
                        "clj".to_string(),
                        "all".to_string()];
        let mut cr = CljRub::new();
        check_cr(&cr);
        let b = Buildable::new(&mut cr, &args);
        let bc = b.get_bc();
        check_bc(bc, &vec!["all"]);
        assert_eq!(b.version(), Ok(0));
    }
}
